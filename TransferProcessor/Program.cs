﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.IO;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Threading;

namespace TransferProcessor
{
    public class TransferData
    {
        public int id;
        public string senderAccountNumber;
        public string receiverAccountNumber;
        public DateTime date;
        public decimal amount;
    }

    class Program
    {
        private static SqlConnection  dbCurrentConnection;
        private static SqlConnection dbBatchConnection;

        private static string incomingBatchFile;
        private static string outgoingTransferFile;

        static void Main(string[] args)
        {
            Console.WriteLine("The application started at {0:HH:mm:ss.fff}", DateTime.Now);

            SetUpConfig();

            Thread t1 = new Thread(new ThreadStart(PerformCurrentTransfer));
            t1.Start();

            Thread t2 = new Thread(new ThreadStart(PerformBatchTransfer));
            t2.Start();

            Console.WriteLine("The application finished at {0:HH:mm:ss.fff}", DateTime.Now);
            Console.ReadLine();
        }

        public static void SetUpConfig()
        {
            dbCurrentConnection = new SqlConnection("user id=sa;" +
                           "password=qweasd1@;"+
                           "server=MW74CDEBV3J5F4\\SQLEXPRESS;" +
                           "Trusted_Connection=yes;" +
                           "database=dev; " +
                           "connection timeout=30");

            dbBatchConnection = new SqlConnection("user id=sa;" +
                           "password=qweasd1@;" +
                           "server=MW74CDEBV3J5F4\\SQLEXPRESS;" +
                           "Trusted_Connection=yes;" +
                           "database=dev; " +
                           "connection timeout=30");

            incomingBatchFile = @"IncommingTransfers\IncommingBatch.txt";
            outgoingTransferFile = @"OutgoingTransfers\";

        }

        private static void PerformCurrentTransfer()
        {
            int fileCount = 20000;

            Stopwatch globalWatch = new Stopwatch();
            Stopwatch localWatch = new Stopwatch();

            long[] localWatchLog = new long[fileCount];

            globalWatch.Start();

            for (int i = 1; i <= fileCount; i++)
            {
                localWatch.Start();

                TransferData transfer = GetOutgoingTransfer(outgoingTransferFile + i + ".txt");
                PerformOutgoingTransfer(transfer);

                localWatch.Stop();
                localWatchLog[i-1] = localWatch.ElapsedMilliseconds;
                localWatch.Reset();
            }

            globalWatch.Stop();

            Console.WriteLine("Min local: {0} ms" + localWatchLog.Min());
            Console.WriteLine("Max local: {0} ms" +  localWatchLog.Max());

            for(int i=0; i < localWatchLog.Length; i++)
                if (localWatchLog[i] > 100)
                    Console.WriteLine(i + "-> " + localWatchLog[i]);
            Console.WriteLine("Single transfer avg : " + localWatchLog.Average() + " ms");
            Console.WriteLine("Single transfer summary : " + (globalWatch.ElapsedMilliseconds) + " ms");
        }

        private static void PerformBatchTransfer()
        {
            Stopwatch globalWatch = new Stopwatch();

            globalWatch.Start();

            List<TransferData> transferDataList = GetIncomingTransfers(incomingBatchFile);
            PerformIncommingTransfer(transferDataList);

            globalWatch.Stop();
            Console.WriteLine("Batch transfers summary : " + (globalWatch.ElapsedMilliseconds) + " ms");
        }

        private static List<TransferData> GetIncomingTransfers(string fileName)
        {
            List<TransferData> incomingTransfers = new List<TransferData>();
            string line;

            try
            {
                using (StreamReader sr = new StreamReader(fileName))
                {
                    while ((line = sr.ReadLine()) != null)
                    {
                        var transfer = ConvertStringToTransferData(line);
                        incomingTransfers.Add(transfer);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return incomingTransfers;
        }

        private static TransferData GetOutgoingTransfer(string fileName)
        {

            String line = string.Empty;

            try
            {
                using (StreamReader sr = new StreamReader(fileName))
                {
                    line = sr.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            TransferData transferData = ConvertStringToTransferData(line);

            return transferData;
        }

        private static TransferData ConvertStringToTransferData(string transfer)
        {
            string[] rawTransferData = transfer.Split(';');

            var transferData = new TransferData()
            {
                id = int.Parse(rawTransferData[0]),
                senderAccountNumber = rawTransferData[1],
                receiverAccountNumber = rawTransferData[2],
                amount = decimal.Parse(rawTransferData[3]),
                date = DateTime.Parse(rawTransferData[4])
            };

            return transferData;
        }


        private static SqlCommand GetUpdateAccountSaldoCommand(string accountNumber, decimal amount)
        {
            SqlParameter accountNumberParameter = new SqlParameter("@accountNumberParameter", System.Data.SqlDbType.VarChar, 28);
            accountNumberParameter.Value = accountNumber;

            SqlParameter amountParameter = new SqlParameter("@amountParameter", System.Data.SqlDbType.Decimal);
            amountParameter.Value = amount;

            SqlCommand UpdateAccountSaldoCommand = new SqlCommand();

            UpdateAccountSaldoCommand.CommandText = "UPDATE [dev].[dbo].[Account] SET Amount = Amount + @amountParameter WHERE Number = @accountNumberParameter;";
            UpdateAccountSaldoCommand.Parameters.Add(accountNumberParameter);
            UpdateAccountSaldoCommand.Parameters.Add(amountParameter);

            return UpdateAccountSaldoCommand;
        }

        private static SqlCommand GetInsertTransferCommand(TransferData transferData)
        {
            SqlParameter idParameter = new SqlParameter("@idParemeter", System.Data.SqlDbType.Int);
            idParameter.Value = transferData.id;

            SqlParameter amountParameter = new SqlParameter("@amountParameter", System.Data.SqlDbType.Decimal);
            amountParameter.Value = transferData.amount;

            SqlParameter senderAccountNumberParameter = new SqlParameter("@senderAccountNumberParameter", System.Data.SqlDbType.VarChar, 28);
            senderAccountNumberParameter.Value = transferData.senderAccountNumber;

            SqlParameter receiverAccountNumberParameter = new SqlParameter("@receiverAccountNumberParameter", System.Data.SqlDbType.VarChar, 28);
            receiverAccountNumberParameter.Value = transferData.receiverAccountNumber;

            SqlParameter transferDateParameter = new SqlParameter("@transferDateParameter", System.Data.SqlDbType.Date, 28);
            transferDateParameter.Value = transferData.date;

            SqlCommand InsertTransferCommand = new SqlCommand();

            InsertTransferCommand.CommandText = "INSERT INTO [dev].[dbo].[ExternalTransfer] (Id, AccountNumber, ExternalAccountNumber, Amount, TransferDateTime, ProcessingDateTime, ExternalBankAccountNumber) values (@idParemeter, @senderAccountNumberParameter, @receiverAccountNumberParameter, @amountParameter, @transferDateParameter, GETDATE(), 0);";
            InsertTransferCommand.Parameters.Add(idParameter);
            InsertTransferCommand.Parameters.Add(amountParameter);
            InsertTransferCommand.Parameters.Add(senderAccountNumberParameter);
            InsertTransferCommand.Parameters.Add(receiverAccountNumberParameter);
            InsertTransferCommand.Parameters.Add(transferDateParameter);

            return InsertTransferCommand;
        }

        private static void PerformOutgoingTransfer(TransferData transferData)
        {
            SqlCommand updateSenderAccountSaldo = GetUpdateAccountSaldoCommand(transferData.senderAccountNumber, -transferData.amount);
            updateSenderAccountSaldo.Connection = dbCurrentConnection;
            SqlCommand updateReceiverAccountSaldo = GetUpdateAccountSaldoCommand(transferData.receiverAccountNumber, transferData.amount);
            updateReceiverAccountSaldo.Connection = dbCurrentConnection;
            SqlCommand insertOutgoingTransfer = GetInsertTransferCommand(transferData);
            insertOutgoingTransfer.Connection = dbCurrentConnection;

            try
            {
                dbCurrentConnection.Open();

                var rowCount1 = updateSenderAccountSaldo.ExecuteNonQuery();
                var rowCount2 = updateReceiverAccountSaldo.ExecuteNonQuery();
                var rowCount3 = insertOutgoingTransfer.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
            }
            finally
            {
                dbCurrentConnection.Close();
            }
        }

        private static void PerformIncommingTransfer(List<TransferData> transferDataList)
        {
            SqlCommand updateSenderAccountSaldo;
            SqlCommand updateReceiverAccountSaldo;

            try
            {
                dbBatchConnection.Open();

                foreach (TransferData transferData in transferDataList)
                {
                    updateSenderAccountSaldo = GetUpdateAccountSaldoCommand(transferData.senderAccountNumber, -transferData.amount);
                    updateSenderAccountSaldo.Connection = dbBatchConnection;
                    updateReceiverAccountSaldo = GetUpdateAccountSaldoCommand(transferData.receiverAccountNumber, transferData.amount);
                    updateReceiverAccountSaldo.Connection = dbBatchConnection;

                    var rowCount1 = updateSenderAccountSaldo.ExecuteNonQuery();
                    var rowCount2 = updateReceiverAccountSaldo.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
            }
            finally
            {
                dbBatchConnection.Close();
            }
        }
    }
}
