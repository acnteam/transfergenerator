﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AS.IBAN;
using AS.IBAN.Model;
using AS.IBAN.Helper;

namespace IBANGenerator
{
    class Program
    {
        static void Main(string[] args)
        {

            IbanGenerator generator = new IbanGenerator();

            string sortCode = "11402004";
            string accountNumber = "3200000123456789";

            try
            {
                var result = generator.GenerateIban(AS.IBAN.Model.ECountry.PL, sortCode, accountNumber);

                Console.WriteLine(result.IBAN.IBAN);
            }
            catch (IbanException e)
            {
                throw e;

            }

            Console.ReadKey();
        }
    }
}
