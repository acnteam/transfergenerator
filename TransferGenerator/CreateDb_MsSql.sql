
CREATE TABLE [dbo].[Account](
	[Number] [char](28) NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
(
	[Number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


CREATE TABLE [dbo].[ExternalTransfer](
	[Id] [int] NOT NULL,
	[AccountNumber] [char](28) NOT NULL,
	[ExternalAccountNumber] [char](28) NOT NULL,
	[ExternalBankAccountNumber] [char](28) NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[TransferDateTime] [datetime] NOT NULL,
	[ProcessingDateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_Transfer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
