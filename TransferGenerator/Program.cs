﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Timers;

namespace TransferGenerator
{
    class Program
    {
        public static string OutgoingTransfersFolder = "OutgoingTransfers";
        public static string IncommingTransfersFolder = "IncommingTransfers";

        public static int OutgoingTransfersCount = 20000;
        public static int IncommingTransfersCount = 20000;

        public static Random _random = new Random(100);

        public static string[] ExternalAccounts =
        {
            "27249083043304637873852818", // Alior - 2490
            "91114043438077385470464990", // mBank - 1140
            "30105077115548560507298462", // ING - 1050
        };

        public static string[] InternalAccounts =
        {
            "36132037045416127816316961", // Pocztowy - 1320
            "81132044374495977493782903", // Pocztowy - 1320
            "37132052588455852091923157", // Pocztowy - 1320
        };

        public static string Separator = ";";

        static void Main(string[] args)
        {

            Console.WriteLine("Witaj w generatore testowych przelewów bankowych!");
            Console.WriteLine(" - Folder z przelewami wychodzącymi to: " + OutgoingTransfersFolder + ".");
            Console.WriteLine(" - Folder z przelewami przychodzącymi to: " + IncommingTransfersFolder + ".");
            Console.WriteLine();

            Console.WriteLine("Masz 2 cele:");
            Console.WriteLine(" - najszybsze przetworzenie wszystkich przelewów przychodzących i wychodzących");
            Console.WriteLine(" - redukacja maksymalnego czasu przetworzenia dla przelewów wychodzących");

            Console.WriteLine();
            Console.WriteLine("Uruchom swoją aplikację do przetwarzania przelewów.");
            Console.WriteLine("Następnie naciśnij dowolny przycisk, aby rozpocząć...");
            Console.ReadKey();
            
            Console.WriteLine();
            if (Directory.Exists(IncommingTransfersFolder))
            {
                Console.WriteLine("Czyszczenie starego folderu " + IncommingTransfersFolder + "...");
                Directory.Delete(IncommingTransfersFolder, true);
            }

            if (Directory.Exists(OutgoingTransfersFolder))
            {
                Console.WriteLine("Czyszczenie starego folderu " + OutgoingTransfersFolder  + "...");
                Directory.Delete(OutgoingTransfersFolder, true);
            }
            
            Console.WriteLine("Tworzenie folderów...");
            Directory.CreateDirectory(OutgoingTransfersFolder);
            Directory.CreateDirectory(IncommingTransfersFolder);

            // Odkomentuj, aby uzyskać efekt równoległości
            //Thread t = new Thread(new ThreadStart(GenerateIncommingTransfers));
            //t.Start();
            GenerateIncommingTransfers();

            Console.WriteLine("Rozpoczęcie przetwarzania plików...");
            for (int i = 1; i < OutgoingTransfersCount + 1; i++)
            {
                if (i % 1000 == 0)
                    Console.WriteLine("Wysłano: " + i + " przelewów.");

                File.WriteAllText(OutgoingTransfersFolder + @"\" + i + ".txt", GenerateRecord(i, false));
            }

            Console.WriteLine("Generowanie przelewów wychodzących zakończone!");
            Console.WriteLine("Naciśnij dowolny przycisk, aby zakończyć...");
            Console.ReadKey();
        }

        private static void GenerateIncommingTransfers()
        {
            //Thread.Sleep(15 * 1000);
            StringBuilder sb = new StringBuilder();

            for (int i = 1; i < IncommingTransfersCount + 1; i++)
                sb.AppendLine(GenerateRecord(OutgoingTransfersCount + i, true));
            
            File.WriteAllText(IncommingTransfersFolder + @"\IncommingBatch.txt", sb.ToString());
            Console.WriteLine("Otrzymano sesję przelewów przychodzących!");
        }

        private static string GenerateRecord(int id, bool IsIncomming)
        {
            int externalAccountId = _random.Next(ExternalAccounts.Length);
            int internalAccountId = _random.Next(InternalAccounts.Length);

            String sourceAccount = IsIncomming ? ExternalAccounts[externalAccountId] : InternalAccounts[internalAccountId];
            String destinationAccount = IsIncomming ? InternalAccounts[internalAccountId] : ExternalAccounts[externalAccountId];

            decimal amount = _random.Next(100000) / 100m;

            // Zakomentuj, aby dodac zmiennosc kwot
            //amount = 1;
            return id + Separator + sourceAccount + Separator + destinationAccount + Separator + amount + Separator + DateTime.Now;
        }
    }
}
