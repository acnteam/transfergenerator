﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace LinqT2SqlSimple
{
    class Program
    {
        public static char Separator = ';';

        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();

            sw.Start();
            TransfersDataClassesDataContext ctx = new TransfersDataClassesDataContext();
            int fileCount = new DirectoryInfo("OutgoingTransfers").EnumerateFiles().Count();
            for (int i = 1; i <= fileCount; i++)
            {
                String[] transferInfo = File.ReadAllText("OutgoingTransfers\\" + i + ".txt").Split(Separator);

                int id = int.Parse(transferInfo[0]);
                string sourceAccount = transferInfo[1];
                string destAccount = transferInfo[2];
                DateTime date = DateTime.Parse(transferInfo[4]);
                decimal transferAmount = decimal.Parse(transferInfo[3]);
                ProcessTransfer(ctx, id, sourceAccount, destAccount, transferAmount, date);
                
                if (i % 1000 == 0)
                    Console.WriteLine("Przetworzono: " + i + " przelewów.");
            }
            String[] incommingTransfers = File.ReadAllLines("IncommingTransfers\\IncommingBatch.txt");

            Console.WriteLine("Przetworzono przelewów przychodzących...");
            foreach (var transfer in incommingTransfers)
            {
                String[] transferInfo = transfer.Split(Separator);

                int id = int.Parse(transferInfo[0]);
                string sourceAccount = transferInfo[2];
                string destAccount = transferInfo[1];
                DateTime date = DateTime.Parse(transferInfo[4]);
                decimal transferAmount = decimal.Parse(transferInfo[3]);
                ProcessTransfer(ctx, id, sourceAccount, destAccount, transferAmount, date);
            }
            sw.Stop();

            Console.WriteLine("Operacja zakończona w: " + sw.ElapsedMilliseconds + " ms.");
            Console.WriteLine("Naciśnij dowolny klawisz...");
            Console.ReadKey();
        }

        private static void ProcessTransfer(TransfersDataClassesDataContext ctx, int id, string sourceAccountNumber, string destAccountNumber, decimal transferAmount, DateTime date)
        {
            string externalBankCode = destAccountNumber.Substring(2, 4);
            string externalBankAccountNumber = "00" + externalBankCode + "00000000000000000000";

            var sourceAccount = ctx.Accounts.FirstOrDefault(a => a.Number == sourceAccountNumber);
            var destAccount = ctx.Accounts.FirstOrDefault(a => a.Number.Substring(2, 4) == externalBankCode);

            ctx.ExternalTransfers.InsertOnSubmit(new
                ExternalTransfer()
            {
                Id = id,
                AccountNumber = sourceAccountNumber,
                ExternalAccountNumber = destAccountNumber,
                ExternalBankAccountNumber = externalBankAccountNumber,
                Amount = transferAmount,
                TransferDateTime = date,
                ProcessingDateTime = DateTime.Now
            });

            sourceAccount.Amount -= transferAmount;
            destAccount.Amount += transferAmount;

            ctx.SubmitChanges();
        }
    }
}
